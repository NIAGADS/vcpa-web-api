-- MySQL dump 10.13  Distrib 5.7.22, for Linux (x86_64)
--
-- Host: localhost    Database: seq_processing
-- ------------------------------------------------------
-- Server version	5.7.22-0ubuntu0.16.04.1-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Current Database: `seq_processing`
--

/*!40000 DROP DATABASE IF EXISTS `seq_processing`*/;

CREATE DATABASE /*!32312 IF NOT EXISTS*/ `seq_processing` /*!40100 DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci */;

USE `seq_processing`;


--
-- Table structure for table `aws_images`
--

DROP TABLE IF EXISTS `aws_images`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `aws_images` (
  `ID` int(11) NOT NULL,
  `ami_id` varchar(10) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `desc` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `seq_projects`
--

DROP TABLE IF EXISTS `seq_projects`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `seq_projects` (
  `project_id` int(11) NOT NULL AUTO_INCREMENT,
  `project_center` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `project_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `project_name_full` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `subproject_name` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `project_desc` text COLLATE utf8mb4_unicode_ci,
  `last_modified` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`project_id`),
  UNIQUE KEY `UNI1` (`project_name`)
) ENGINE=InnoDB AUTO_INCREMENT=112 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `seq_runs`
--

DROP TABLE IF EXISTS `seq_runs`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `seq_runs` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `project_id` int(11) NOT NULL,
  `sample_id` int(11) NOT NULL,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `UNI1` (`ID`,`project_id`,`sample_id`),
  KEY `IND1` (`project_id`,`sample_id`)
) ENGINE=InnoDB AUTO_INCREMENT=12706 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `seq_sample_attributes`
--

DROP TABLE IF EXISTS `seq_sample_attributes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `seq_sample_attributes` (
  `sample_attribute_id` int(11) NOT NULL AUTO_INCREMENT,
  `attribute_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `attribute_type` enum('BOOL','INT','DECIMAL','TEXT','TIMESTAMP','VARCHAR') COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `attribute_desc` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `disabled` int(1) DEFAULT NULL,
  `last_modified` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`sample_attribute_id`)
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `seq_sample_attributes_BOOL`
--

DROP TABLE IF EXISTS `seq_sample_attributes_BOOL`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `seq_sample_attributes_BOOL` (
  `ID` int(1) unsigned NOT NULL AUTO_INCREMENT,
  `project_id` int(10) unsigned NOT NULL,
  `sample_id` int(10) unsigned NOT NULL,
  `attr_id` int(10) unsigned NOT NULL,
  `sample_attr_value` int(1) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `index2` (`project_id`,`sample_id`,`attr_id`),
  KEY `FK1` (`sample_id`),
  CONSTRAINT `FK1` FOREIGN KEY (`sample_id`) REFERENCES `seq_samples` (`sample_id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=33311 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `seq_sample_attributes_BOOL_bkp`
--

DROP TABLE IF EXISTS `seq_sample_attributes_BOOL_bkp`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `seq_sample_attributes_BOOL_bkp` (
  `ID` int(1) unsigned NOT NULL AUTO_INCREMENT,
  `project_id` int(10) unsigned NOT NULL,
  `sample_id` int(10) unsigned NOT NULL,
  `attr_id` int(10) unsigned NOT NULL,
  `sample_attr_value` int(1) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `index2` (`project_id`,`sample_id`,`attr_id`)
) ENGINE=InnoDB AUTO_INCREMENT=24250 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `seq_sample_attributes_INT`
--

DROP TABLE IF EXISTS `seq_sample_attributes_INT`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `seq_sample_attributes_INT` (
  `ID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `project_id` int(10) unsigned NOT NULL,
  `sample_id` int(10) unsigned NOT NULL,
  `attr_id` int(10) unsigned NOT NULL,
  `sample_attr_value` bigint(13) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `index2` (`project_id`,`sample_id`,`attr_id`)
) ENGINE=InnoDB AUTO_INCREMENT=95188 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `seq_sample_attributes_VARCHAR`
--

DROP TABLE IF EXISTS `seq_sample_attributes_VARCHAR`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `seq_sample_attributes_VARCHAR` (
  `ID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `project_id` int(10) unsigned NOT NULL,
  `sample_id` int(10) unsigned NOT NULL,
  `attr_id` int(10) unsigned NOT NULL,
  `sample_attr_value` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `index2` (`project_id`,`sample_id`,`attr_id`)
) ENGINE=InnoDB AUTO_INCREMENT=74629 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `seq_samples`
--

DROP TABLE IF EXISTS `seq_samples`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `seq_samples` (
  `sample_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `project_id` int(10) unsigned NOT NULL,
  `sample_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `subject_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `LSAC` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `seq_type` enum('WES','WGS','') COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `isDropped` int(1) unsigned DEFAULT '0',
  `last_modified` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`sample_id`),
  UNIQUE KEY `UNI1` (`sample_id`,`project_id`),
  UNIQUE KEY `sample_name_UNIQUE` (`sample_name`)
) ENGINE=InnoDB AUTO_INCREMENT=30015 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`localhost`*/ /*!50003 TRIGGER `seq_processing`.`seq_samples_AFTER_DELETE` AFTER DELETE ON `seq_samples` FOR EACH ROW
BEGIN
  DELETE FROM seq_sample_attributes_VARCHAR WHERE sample_id = OLD.sample_id AND project_id=OLD.project_id;
  DELETE FROM seq_sample_attributes_INT WHERE sample_id = OLD.sample_id AND project_id=OLD.project_id;
  DELETE FROM seq_sample_attributes_BOOL WHERE sample_id = OLD.sample_id AND project_id=OLD.project_id;

END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;

--
-- Table structure for table `seq_tracking_processing_steps`
--

DROP TABLE IF EXISTS `seq_tracking_processing_steps`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `seq_tracking_processing_steps` (
  `track_step_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `step_name_short` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `step_name_full` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `step_type` enum('BOOL','INT','DECIMAL','TEXT','TIMESTAMP','VARCHAR') COLLATE utf8mb4_unicode_ci NOT NULL,
  `step_desc` text COLLATE utf8mb4_unicode_ci,
  `step_phase_name` enum('stage 1','stage 2a','stage 2b','stage 2c','stage 3a','stage 3b','stage 2b-forced','','stage 0') COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `step_stage` enum('stage 1','stage 2','stage 3','','stage 0') COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `col_index` int(11) DEFAULT '0',
  `disabled` int(1) NOT NULL DEFAULT '0',
  `last_modified` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`track_step_id`),
  UNIQUE KEY `step_name_short` (`step_name_short`,`step_type`),
  UNIQUE KEY `index3` (`step_name_full`,`step_type`)
) ENGINE=InnoDB AUTO_INCREMENT=181 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `seq_tracking_processing_steps_BOOL`
--

DROP TABLE IF EXISTS `seq_tracking_processing_steps_BOOL`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `seq_tracking_processing_steps_BOOL` (
  `ID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `run_id` int(10) unsigned DEFAULT NULL,
  `tracking_step_id` int(10) unsigned NOT NULL,
  `tracking_value` int(1) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `index2` (`tracking_step_id`,`run_id`)
) ENGINE=InnoDB AUTO_INCREMENT=14703 DEFAULT CHARSET=ascii;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `seq_tracking_processing_steps_DECIMAL`
--

DROP TABLE IF EXISTS `seq_tracking_processing_steps_DECIMAL`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `seq_tracking_processing_steps_DECIMAL` (
  `ID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `run_id` int(10) unsigned DEFAULT NULL,
  `tracking_step_id` int(10) unsigned NOT NULL,
  `tracking_value` decimal(18,6) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `index2` (`tracking_step_id`,`run_id`)
) ENGINE=InnoDB AUTO_INCREMENT=396422 DEFAULT CHARSET=ascii;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `seq_tracking_processing_steps_INT`
--

DROP TABLE IF EXISTS `seq_tracking_processing_steps_INT`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `seq_tracking_processing_steps_INT` (
  `ID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `run_id` int(10) unsigned DEFAULT NULL,
  `tracking_step_id` int(10) unsigned NOT NULL,
  `tracking_value` bigint(13) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `index2` (`tracking_step_id`,`run_id`)
) ENGINE=InnoDB AUTO_INCREMENT=620641 DEFAULT CHARSET=ascii;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `seq_tracking_processing_steps_TIMESTAMP`
--

DROP TABLE IF EXISTS `seq_tracking_processing_steps_TIMESTAMP`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `seq_tracking_processing_steps_TIMESTAMP` (
  `ID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `run_id` int(10) unsigned DEFAULT NULL,
  `tracking_step_id` int(10) unsigned NOT NULL,
  `tracking_value` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `index2` (`tracking_step_id`,`run_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3128875 DEFAULT CHARSET=ascii;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `seq_tracking_processing_steps_VARCHAR`
--

DROP TABLE IF EXISTS `seq_tracking_processing_steps_VARCHAR`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `seq_tracking_processing_steps_VARCHAR` (
  `ID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `run_id` int(10) unsigned DEFAULT NULL,
  `tracking_step_id` int(10) unsigned NOT NULL,
  `tracking_value` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `index2` (`tracking_step_id`,`run_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3380614 DEFAULT CHARSET=ascii;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `seq_tracking_qc_steps`
--

DROP TABLE IF EXISTS `seq_tracking_qc_steps`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `seq_tracking_qc_steps` (
  `qc_step_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `qc_step_name_short` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  `qc_step_name_full` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `qc_step_type` enum('BOOL','INT','DECIMAL','TEXT','TIMESTAMP','VARCHAR') COLLATE utf8mb4_unicode_ci NOT NULL,
  `qc_step_desc` text COLLATE utf8mb4_unicode_ci,
  `last_modified` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`qc_step_id`),
  UNIQUE KEY `qc_step_name_short` (`qc_step_name_short`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `seq_tracking_revision_steps_BOOL`
--

DROP TABLE IF EXISTS `seq_tracking_revision_steps_BOOL`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `seq_tracking_revision_steps_BOOL` (
  `ID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `project_id` int(10) unsigned NOT NULL,
  `sample_id` int(12) unsigned NOT NULL,
  `tracking_step_id` int(10) unsigned NOT NULL,
  `tracking_value` int(1) DEFAULT NULL,
  `last_modified` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=ascii;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `seq_tracking_revision_steps_DECIMAL`
--

DROP TABLE IF EXISTS `seq_tracking_revision_steps_DECIMAL`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `seq_tracking_revision_steps_DECIMAL` (
  `ID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `project_id` int(10) unsigned NOT NULL,
  `sample_id` int(12) unsigned NOT NULL,
  `tracking_step_id` int(10) unsigned NOT NULL,
  `tracking_value` decimal(18,6) DEFAULT NULL,
  `last_modified` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=ascii;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `seq_tracking_revision_steps_INT`
--

DROP TABLE IF EXISTS `seq_tracking_revision_steps_INT`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `seq_tracking_revision_steps_INT` (
  `ID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `project_id` int(10) unsigned NOT NULL,
  `sample_id` int(12) unsigned NOT NULL,
  `tracking_step_id` int(10) unsigned NOT NULL,
  `tracking_value` bigint(13) DEFAULT NULL,
  `last_modified` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=ascii;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `seq_tracking_revision_steps_TIMESTAMP`
--

DROP TABLE IF EXISTS `seq_tracking_revision_steps_TIMESTAMP`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `seq_tracking_revision_steps_TIMESTAMP` (
  `ID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `project_id` int(10) unsigned NOT NULL,
  `sample_id` int(12) unsigned NOT NULL,
  `tracking_step_id` int(10) unsigned NOT NULL,
  `tracking_value` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `last_modified` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=ascii;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `seq_tracking_revision_steps_VARCHAR`
--

DROP TABLE IF EXISTS `seq_tracking_revision_steps_VARCHAR`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `seq_tracking_revision_steps_VARCHAR` (
  `ID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `project_id` int(10) unsigned NOT NULL,
  `sample_id` int(12) unsigned NOT NULL,
  `tracking_step_id` int(10) unsigned NOT NULL,
  `tracking_value` varchar(255) DEFAULT NULL,
  `last_modified` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=ascii;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `seq_wes_interval_files`
--

DROP TABLE IF EXISTS `seq_wes_interval_files`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `seq_wes_interval_files` (
  `ID` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `desc` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `s3_path` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `last_modified` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Temporary table structure for view `vw_samples_lacking_upload`
--

DROP TABLE IF EXISTS `vw_samples_lacking_upload`;
/*!50001 DROP VIEW IF EXISTS `vw_samples_lacking_upload`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE VIEW `vw_samples_lacking_upload` AS SELECT 
 1 AS `sample_id`,
 1 AS `project_id`,
 1 AS `sample_name`,
 1 AS `subject_name`,
 1 AS `LSAC`,
 1 AS `seq_type`,
 1 AS `isDropped`,
 1 AS `last_modified`*/;
SET character_set_client = @saved_cs_client;

--
-- Final view structure for view `vw_samples_lacking_upload`
--

/*!50001 DROP VIEW IF EXISTS `vw_samples_lacking_upload`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `vw_samples_lacking_upload` AS select `SM`.`sample_id` AS `sample_id`,`SM`.`project_id` AS `project_id`,`SM`.`sample_name` AS `sample_name`,`SM`.`subject_name` AS `subject_name`,`SM`.`LSAC` AS `LSAC`,`SM`.`seq_type` AS `seq_type`,`SM`.`isDropped` AS `isDropped`,`SM`.`last_modified` AS `last_modified` from (`seq_samples` `SM` left join `seq_sample_attributes_VARCHAR` `SMVR` on((`SM`.`sample_id` = `SMVR`.`sample_id`))) where ((`SM`.`project_id` in (100,101)) and isnull(`SMVR`.`sample_attr_value`)) order by `SM`.`project_id`,`SM`.`LSAC` */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2018-04-25 10:57:54
