## USAGE

Start by running [docker-compose](https://docs.docker.com/compose/install/) inside the directory containing docker-compose.yml

1. `git clone --recurse-submodules https://bitbucket.org/NIAGADS/vcpa-web-api.git`
2. `cd vcpa-web-api`
3. `docker-compose up`

### If you need to rebuild image: `docker-compose up --build` 
### If you need to remove all the images: `docker-compose down --rmi all`

---

## Accessing running instance

Your instance of the api will accessible via localhost or public IP

[http://localhost/v1/projects]


---

## Adding data

### Follow the steps below to add data


1. View projects: get "/v1/projects"

   #### `curl -sS "http://localhost/v1/projects"`

2. View the project by name: get "/v1/projects/${name}" Request: `${project_name}`

   #### `curl -sS "http://localhost/v1/projects/${project_name}"`
	   
3. Add project: post "/v1/projects/add" 
                Request: `-d project_name` 
				Optional: `-d project_name_full`, `-d project_desc`, `-d project_center`, `-d subproject_name` 
                
   #### `curl -sS -d project_name=${project_name} -d project_name_full=${project_name_full} -d project_desc=${project_desc} "http://localhost/v1/projects/add"`
	   
4. Add sample: post "/v1/projects/sample/add" 
               Request: `-d project_id`, `-d sample_name` 
			   Optional: `-d subject_name`, `-d LSAC`, `-d seq_type` 
               
   #### `curl -sS -d project_id=${project_id} -d sample_name=${sample_name} -d project_desc=${project_desc} -d subject_name=${subject_name} -d LSAC=${LSAC} -d seq_type=${seq_type} "http://localhost/v1/projects/sample/add"`

5. View sample by ID: get "/v1/projects/samples/by-project-id/${id}"
                      Request: `id`
   #### `curl -sS  "http://localhost/v1/projects/samples/by-project-id/${id}"`
					 

6. Add sample by PATH: post "/v1/sample/set-attr/seqfile_s3_uri/${project_id}/${sample_name}/${S3_path}"
                       Request: `${project_id}`, `${sample_name}`, `${S3_path}`
   #### ${S3_path} is the URL-ENCODE path to submit S3 address.
   #### `curl -sS "http://localhost/v1/sample/set-attr/seqfile_s3_uri/${project_id}/${sample_name}/${S3_path}"`
